(function () {
  'use strict';

  angular.module('contactsApp', [
    'ui.router',
    'ngAnimate',
    'ngStorage',
    'angularUtils.directives.dirPagination',
    'angular-loading-bar'
  ])
    .config(($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, cfpLoadingBarProvider) => {

        // set loading latency for appearing
        cfpLoadingBarProvider.latencyThreshold = 0;

        //disable case sensitivity for urls
        $urlMatcherFactoryProvider.caseInsensitive(true);

        //another version for case insensitivity
        /*
         $urlRouterProvider.rule(function ($injector, $location) {
         var path = $location.path(), normalized = path.toLowerCase();
         if (path != normalized) {
         $location.replace().path(normalized);
         }
         });
         */

        $stateProvider
          .state('index', {
            url: '/',
            templateUrl: 'js/auth/index-auth/index-auth.html',
            controller: 'IndexAuthController'
          })
          .state('login', {
            url: '/login',
            templateUrl: 'js/auth/login/login.html',
            controller: 'LoginController'
          })
          .state('register', {
            url: '/register',
            templateUrl: 'js/auth/register/register.html',
            controller: 'RegisterController'
          })
          .state('home', {
            url: '/home',
            templateUrl: 'js/home/home.html',
            controller: 'HomeController',
            abstract: true
          })
          .state('home.profile', {
            url: '/profile',
            templateUrl: 'js/profile/profile.html',
            controller: 'ProfileController'
          })
          .state('home.invitation', {
            url: '/invitation',
            templateUrl: 'js/invitation/invitation.html',
            controller: 'InvitationController'
          })
          .state('home.contacts', {
            url: '/contacts/:itemsPage/:pageNumber',
            templateUrl: 'js/contacts-list/contacts.html'
          })
          .state('home.detail', {
            url: '/contacts/:contactId',
            templateUrl: 'js/contacts-detail/contacts-detail.html',
            controller: 'ContactsDetailsController'
          })
          .state('home.detail.edit', {
            url: '/edit',
            templateUrl: 'js/edit/edit.html',
            controller: 'EditInfoController'
          })
          .state('home.translate', {
            url: '/translate',
            templateUrl: 'js/translate/translate.html',
            controller: 'TranslateController'
          });

        //default redirecting
        $urlRouterProvider.otherwise("/home/invitation");
      })
    .run(($rootScope, $state, $stateParams) => {

          $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            //if (toState.name === 'home.profile' && !storageService.isLoggedIn()) {
            //  $state.go('home.invitation');
            //}
          });

          //helpers for checking state's name & state's params
          //it displays on the page all the time
          $rootScope.user = '';
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;

        }
    );


})();