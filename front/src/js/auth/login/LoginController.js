'use strict';

angular.module('contactsApp')
  .controller('LoginController',
    ($scope, popupService, $location, authService) => {

      $scope.login = () => {

        if ($scope.loginForm.$invalid) {
          return;
        }

        let
          email = $scope.user.email,
          password= $scope.user.password;

        authService.login({
          email: email,
          password: password
        }).then(user => $location.url('/home/profile'))
          .catch(err => {
            popupService.open('popupAlert', {
              title: 'Error',
              content: err.data.message
            });
          });
      };

    }
  );