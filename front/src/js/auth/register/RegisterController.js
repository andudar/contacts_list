'use strict';

angular.module('contactsApp')
  .controller('RegisterController', ($scope, popupService, $location, authService) => {
      "ngInject";

      $scope.register = () => {

        if ($scope.registerForm.$invalid) {
          return;
        }

        let credentials = {
          name: $scope.user.name,
          email: $scope.user.email,
          password: $scope.user.password
        };

        authService.register(credentials)
          .then(user => {
            popupService.open('popupAlert', {
              title: 'Registration',
              content: 'You will get an email with confirmation of your registration!'
            });
            $location.url('/home/profile')
          })
          .catch(err => {
            popupService.open('popupAlert', {
              title: 'Error',
              content: err.data.message
            });
          });
      };

    }
  );