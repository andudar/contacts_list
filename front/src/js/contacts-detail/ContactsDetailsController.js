'use strict';

angular.module('contactsApp')
  .controller('ContactsDetailsController',
    ($scope, $stateParams, apiService, popupService, $location) => {

      //preventing access to home/contacts url without params
      if (!$stateParams.contactId) {
        $location.url('/invitation')
      }

      apiService.contacts.getPersonDetails($stateParams.contactId)
        .then(person => $scope.person = person)
        .catch(err => {
          popupService.open('popupAlert', {
            title: 'Error',
            content: `Can't find this user`
          });
          //redirect user in case user id is invalid
          $location.url('/invitation');
        });

      $scope.update = () => {
        $scope.editing = false;
        apiService.contacts.updateInfo($scope.person._id, $scope.person)
          .then(res => {
            popupService.open('popupAlert', {
              title: 'Updated',
              content: `Info for user ${$scope.person.name} ${$scope.person.lastName} has been successfully updated`
            });
          })
          .catch(err => {
            popupService.open('popupAlert', {
              title: 'Error',
              content: err.data.message
            });
          })
      };

      $scope.remove = () => {
        popupService.open('popupConfirm', {
          title: 'Confirm deletion',
          content: `Do you really want to delete ${$scope.person.name} ${$scope.person.lastName}?`,
          yes: () => {
            popupService.clear();
            apiService.contacts.removePerson($scope.person._id)
              .then(() => $location.url('/contacts'))
              .catch(err => {
                popupService.open('popupAlert', {
                  title: 'Error',
                  content: `Can't delete user`
                })
              })
          }
        })
      }

    }
  );
