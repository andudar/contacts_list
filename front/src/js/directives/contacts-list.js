'use strict';
angular.module('contactsApp')
  .directive('contactsList', [() => {
    return {
      restrict: 'E',
      controller: Controller,
      templateUrl: 'js/components/contacts/contacts-list/contacts-list.html'
    };

    function Controller($state, $scope, popupService, apiService, $rootScope) {
      //handling visibility of contacts block depending on loading status
      var contactsListHolder = $('.contacts-container');
      //event handlers for angular-loading-bar
      $rootScope.$on('cfpLoadingBar:completed', () => {
        contactsListHolder.addClass('loaded');
      });

      $rootScope.$on('cfpLoadingBar:started', () => {
        contactsListHolder.removeClass('loaded');
      });

      //initialize default counters for pagination
      $scope.pageNumber = 1;
      $scope.total_count = 0;
      $scope.itemsPage = '6';

      $scope.save = () => {
        if (!$scope.newPerson.name || !$scope.newPerson.lastName) {
          return popupService.open('popupAlert', {
            title: 'Error with adding new person to the list',
            content: `Can't add person without name or last name`
          })
        }
        apiService.contacts.addPerson($scope.newPerson)
          .then(res => {
            $scope.people.push(res);
            $scope.newPerson = '';
            $state.reload();
          }, err => {
            popupService.open('popupAlert', {
              title: `Error: ${err.status}`,
              content: err.data.message
            })
          });
      };

      $scope.remove = index => {
        var
          person = $scope.people[index];

        popupService.open('popupConfirm', {
          title: 'Confirm deletion',
          content: `Do you really want to delete ${person.name} ${person.lastName}?`,
          yes: () => {
            popupService.clear();
            apiService.contacts.removePerson(person._id)
              .then(() => {
                $scope.people.splice(index, 1);
                $state.reload();
              })
              .catch(err => {
                popupService.open('popupAlert', {
                  title: `Error: ${err.status}`,
                  content: err.statusText
                })
              })
          }
        });
      };

      $scope.getList = pageNumber => {
        //for url's changing depends on state
        $state.go('.', {pageNumber: pageNumber, itemsPage: $scope.itemsPage}, {notify: false});

        apiService.contacts.getPeopleListPagination($scope.itemsPage, pageNumber)
          .then(data => {
            $scope.people = data.data;
            $scope.total_count = data.length;
          })
          .catch(err => {
            popupService.open('popupAlert', {
              title: `Error: ${err.status}`,
              content: err.statusText
            })
          });
      };
      $scope.getList($scope.pageNumber);
    }
  }]);
