/**
 * Created by andrii on 08.09.16.
 */
'use strict';
angular.module('contactsApp')
  .directive('passwordValidation', () => {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: ($scope, element, attr, Ctrl) => {
      const Validation = value => {
        //at least one lowercase char, one uppercase char and one number
        // length between 4 - 8
        const regExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}/;

        if (regExp.test(value)) {
          Ctrl.$setValidity('passwordValidation', true);
        } else {
          Ctrl.$setValidity('passwordValidation', false);
        }
        return value;
      };
      Ctrl.$parsers.push(Validation);
    }
  };
});