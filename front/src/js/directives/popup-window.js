'use strict';
angular.module('contactsApp')
  .directive('popupWindow', ['popupService', popupService => {
    return {
      restrict: 'E',
      template: [
        '<div class="popup" ng-mousedown="popupWindow.clear() && popupWindow.close()" ng-class="{opened : popupWindow.isVisible}">',
        '<div class="popup-content" ng-mousedown="$event.stopPropagation()" ng-include="popupWindow.templateUrl"></div>',
        '</div>'
      ].join(''),

      link: function (scope) {
        scope.popupWindow = popupService;
        scope.closePopUp = function () {
          scope.popupWindow.isVisible = false;
          scope.popupWindow.templateUrl = '';
        };
      }
    };
  }]);
