'use strict';

angular.module('contactsApp')
  .controller('HomeController', ($scope, $http, apiService, authService, storageService) => {

    $scope.message = "This is our home!";

    $scope.isLoggedIn = storageService.isLoggedIn;

    $scope.logout = () => storageService.reset();

  });