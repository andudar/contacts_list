'use strict';

angular.module('contactsApp')
  .controller('ProfileController', ($scope, apiService, popupService, $location, $state) => {

      apiService.auth.getProfile()
        .then(data => $scope.user = data)
        .catch(err => {
          if (err.status == 401) {
            popupService.open('popupAlert', {
              title: `Error: ${err.status}`,
              content: err.statusText
            });

          } else {
            popupService.open('popupAlert', {
              title: 'Error',
              content: `Something bad happened, try later`
            })
          }
          $state.go('home.invitation');
        });

    }
  );