'use strict';

    angular.module('contactsApp')
      .service('apiService', ($http, storageService, removeDataPropService) => {

        const apiCalls = {

          contacts: {
            getPeopleList: () => $http.get('./contacts/'),
            getPeopleListPagination: (itemsPerPage, pageNumber) => $http.get(`./contacts/${itemsPerPage}/${pageNumber}`),
            getPersonDetails: id => $http.get(`./contacts/${id}`),
            addPerson: obj => $http.post(`./contacts`, obj),
            removePerson: id => $http.delete(`./contacts/${id}`),
            updateInfo: (id, obj) => $http.put(`./contacts/${id}`, obj)
          },

          auth: {
            getProfile: () => $http.get('./profile', {
              headers: {
                Authorization: 'Bearer ' + storageService.getToken()
              }
            }),
            login: obj => $http.post(`/login`, obj),
            register: obj => $http.post(`/register`, obj)
          },

          features: {
            translate: (lang, phrase) => $http.post(`./translate?lang=${lang}&phrase=${phrase}`)
          }

        };

        //return clear data
        return removeDataPropService(apiCalls);
      });
