angular.module('contactsApp')
  .service('authService', (storageService, apiService) => {

      var _register = user => apiService.auth.register(user).then(data => storageService.saveToken(data.token));

      var _login = user => apiService.auth.login(user).then(data => storageService.saveToken(data.token));

      var _logout = () => apiService.auth.logout().then(() => storageService.reset());

      return {
        register: _register,
        login: _login,
        logout: _logout
      };
    });