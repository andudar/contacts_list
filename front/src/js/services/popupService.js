'use strict';

angular.module('contactsApp').service('popupService', function () {

  this.isVisible = false;
  this.templateUrl = null;
  this.messageCash = [];


  var templates = {
    popupConfirm: 'js/components/popups/popup-confirm.html',
    popupAlert: 'js/components/popups/popup-alert.html',
  };

  this.open = function (template, params) {

    this.params = params || {};

    if (this.templateUrl) {
      this.messageCash.push({template: template, params: this.params});
      return;
    }

    this.fields = this.params.fields || {};
    this.isVisible = true;

    this.templateUrl = templates[template];
  };

  this.close = function (keepParams) {
    var
      closeEvent = this.params.closeEvent;

    this.params = keepParams ? this.params : {};
    this.isVisible = false;
    this.templateUrl = null;

    if (closeEvent) {
      closeEvent();
    }

    if (this.messageCash.length) {
      var currentParam = this.messageCash.pop();
      this.open(currentParam.template, currentParam.params);
    }
  };

  this.clear = function () {
    this.messageCash = [];

    return true;
  };

});
