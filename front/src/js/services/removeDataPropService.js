'use strict';

angular.module('contactsApp')
  .factory('removeDataPropService', () => {

    // return proxies for api calls object
    return api => {

      let
        proxy = {},
        _getData = res => res.data;

      Object.keys(api).forEach(key => {

        proxy[key] = api[key];

        for (let prop in api[key]) if (api[key].hasOwnProperty(prop)) {

          //for each api call create proxy with added getData function
          proxy[key][prop] = new Proxy(api[key][prop], {
            //proxy handler instead of direct access to the api method
            apply: function (target, thisArg, argumentsList) {
              return target.apply(thisArg, argumentsList).then(_getData);
            }
          })

        }
      });

      return proxy;

    };

  });
