"use strict";

angular.module('contactsApp').service('storageService', ($localStorage, $window) => {


    var _saveToken = function (token) {
      $localStorage.token = token;
    };

    var _getToken = function () {
      return $localStorage.token;
    };

    var _reset = function () {
      $localStorage.$reset();
    };

    var _isLoggedIn = function () {
      var token = _getToken();
      var payload;

      if (token) {
        payload = $window.atob(token.split('.')[1]);
        payload = JSON.parse(payload);

        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    };

    var _currentUser = function () {
      if (_isLoggedIn()) {
        var
          token = _getToken(),
          payload;

        payload = $window.atob(token.split('.')[1]);
        payload = JSON.parse(payload);

        return {
          email: payload.email,
          name: payload.name
        };
      }
    };


    return {
      saveToken: _saveToken,
      getToken: _getToken,
      reset: _reset,
      isLoggedIn: _isLoggedIn,
      currentUser: _currentUser
    };

  });
