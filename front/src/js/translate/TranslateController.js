'use strict';

angular.module('contactsApp')
  .controller('TranslateController', ($scope, apiService) => {


      $scope.phrase = '';
      $scope.lang = '';


      $scope.translate = () => {
        //preventing unnecessary api calls
        if (!$scope.lang || !$scope.phrase) {
          return;
        }

        apiService.features.translate($scope.lang, $scope.phrase).then(data => {
          $scope.translation = data.translation;
          $scope.langTranslated = data.from;
        });
      };

      //translate immediately when language has changed
      $scope.$watch('lang', () => {
        $scope.translate()
      })

    }
  );