/**
 * Created by andrii on 12.08.16.
 */
module.exports = [
  'front/src/bower_components/angular/angular.js',
  'front/src/bower_components/angular-ui-router/release/angular-ui-router.js',
  'front/src/bower_components/angular-animate/angular-animate.js',
  'front/src/bower_components/jquery/dist/jquery.js',
  'front/src/bower_components/bootstrap/js/tooltip.js',
  'front/src/bower_components/ngstorage/ngStorage.js',
  'front/src/bower_components/angularUtils-pagination/dirPagination.js',
  'front/src/bower_components/angular-loading-bar/build/loading-bar.min.js',
  'front/src/bower_components/proxy-polyfill/proxy.min.js',
];
