/**
 * Created by andrii on 12.08.16.
 */
module.exports = {
  build: {
    html: 'front/build/',
    js: 'front/build/js/',
    css: 'front/build/css/',
    images: 'front/build/assets/images/',
    fonts: 'front/build/assets/fonts/'
  },
  src: {
    //exception for unnecessary html copy for pagination directive
    html: ['front/src/**/*.html', '!front/src/**/*.tpl.html'],
    js: 'front/src/js/**/*.js',
    styles: [
      'front/src/styles/main.scss',
      'front/src/styles/**/*'
    ],
    fonts: 'front/src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2}',
    images: 'front/src/assets/images/**/*.{jpg,png,svg,gif,ico}',
  },
  watch: {
    html: 'front/src/**/*.html',
    js: 'front/src/js/**/*.js',
    style: 'front/src/**/*.{css,scss}',
    fonts: 'front/src/assets/fonts/*.{eot,svg,ttf,woff,woff2}',
    images: 'front/src/assets/images/**/*.{jpg,png,svg,gif,ico}'
  },
  clean: 'front/build/*',
  docs: {
    frontAll: [
      'front/src/js/*.js',
      'front/src/js/**/*.js'
    ],
    frontDoc: 'docs/front',
    serverAll: [
      'server/*.js',
      'server/**/*.js',
      '!server/node_modules/**/*.js'
    ],
    serverDoc: 'docs/server',
    docsClean: 'docs/*'
  }
};
