"use strict";
const del = require('del');

module.exports =  (options) => {
  return function () {
    return del([options.clean])
  }
};