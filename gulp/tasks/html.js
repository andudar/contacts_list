/**
 * Created by andrii on 20.08.16.
 */
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const combiner = require('stream-combiner2').obj;
const argv = require('yargs').argv;

module.exports =  options => {
  return function () {
    return combiner(
      gulp.src(options.src, {since: gulp.lastRun('html')}),
      $.newer(options.dest),
      $.if((!argv.dev), $.htmlmin({
        collapseWhitespace: true
      })),
      $.if((argv.dev), $.debug({title: 'html'})),
      gulp.dest(options.dest),
      $.livereload()).on('error', err => {
      console.log(`task: html \nname: ${err.name} \nmessage: ${err.message} \nplugin: ${err.plugin}`);
    });
  }

};