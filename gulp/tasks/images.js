/**
 * Created by andrii on 20.08.16.
 */

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const combiner = require('stream-combiner2').obj;
const argv = require('yargs').argv;

module.exports =  options => {
  return () => {
    return combiner(gulp.src(options.src)
      .pipe($.newer(options.dest))
      .pipe($.if(!(argv.dev), $.imagemin({
        progressive: true
      })))
      .pipe($.if((argv.dev), $.debug({title: 'images'})))
      .pipe(gulp.dest(options.dest))
      .pipe($.livereload())).on('error', err => {
      console.log(`task: js \nname: ${err.name} \nmessage: ${err.message} \nplugin: ${err.plugin}`);
  });
  }

};