/**
 * Created by andrii on 20.08.16.
 */
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const combiner = require('stream-combiner2').obj;
const argv = require('yargs').argv;

module.exports =  options => {
  return function () {
    return combiner(gulp.src(options.src),
      $.if((argv.dev), $.sourcemaps.init()),
      $.babel({
        presets: ['es2015'],
        ignore: ['/bower_components/']
      }),
      $.ngAnnotate(),
      $.concat('main.min.js'),
      $.uglify(),
      $.if((argv.dev), combiner(
        $.sourcemaps.write('.'),
        $.debug({title: 'js'}))
      ),
      gulp.dest(options.dest),
      $.livereload()).on('error', err => {
      console.log(`task: js \nname: ${err.name} \nmessage: ${err.message} \nplugin: ${err.plugin}`);
    });
  }

};