/**
 * Created by andrii on 20.08.16.
 */
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const argv = require('yargs').argv;
const libs = require('./../libs_paths');

module.exports =  options => {
  return function () {
    return gulp.src(libs)
      .pipe($.concat('libs.js'))
      .pipe($.if((argv.dev), $.sourcemaps.init()))
      .pipe($.concat('libs.js'))
      .pipe($.uglify())
      .pipe($.if((argv.dev),$.sourcemaps.write('.')))
      .pipe(gulp.dest(options.dest));
  }
};