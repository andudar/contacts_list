/**
 * Created by andrii on 20.08.16.
 */

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const combiner = require('stream-combiner2').obj;
const argv = require('yargs').argv;

module.exports =  options => {
  return function () {
    return combiner(gulp.src(options.src),
      $.if((argv.dev), $.sourcemaps.init()),
      $.sass({includePaths: ['./assets']}),
      $.autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      }),
      $.concat('main.min.css'),
      $.cssmin(),
      $.if((argv.dev), combiner(
        $.sourcemaps.write('.'),
        $.debug({title: 'styles'}))
      ),
      gulp.dest(options.dest),
      $.livereload()).on('error', err => {
      console.log(`task: styles \nname: ${err.name} \nmessage: ${err.message} \nplugin: ${err.plugin}`);
    });
  }

};