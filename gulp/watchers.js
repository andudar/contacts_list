/**
 * Created by andrii on 12.08.16.
 */

var
  paths = require('./paths'),
  libs = require('./libs_paths');

module.exports = {
  html: {path: [paths.watch.html], task: () => 'html'},
  style: {path: [paths.watch.style], task: () => 'styles'},
  js: {path: [paths.watch.js], task: () => 'js'},
  libs: {path: libs, task: () => 'libs'},
  images: {path: [paths.watch.images], task: () => 'images'},
  fonts: {path: [paths.watch.fonts], task: () => 'fonts'},
};
