/**
 * Created by andrii on 12.08.16.
 */
'use strict';

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const paths = require('./gulp/paths');
const watchers = require('./gulp/watchers');

let lazyRequireTask = (taskName, pathToTask, options) => {
  options = options || {};

  gulp.task(taskName, () => {
    let task = require(pathToTask).call(this, options);
    return task();
  })
};


lazyRequireTask('styles', './gulp/tasks/styles', {
  src: paths.src.styles,
  dest: paths.build.css
});


lazyRequireTask('js', './gulp/tasks/js', {
  src: paths.src.js,
  dest: paths.build.js
});


lazyRequireTask('clean', './gulp/tasks/front_clean', {
  clean: paths.clean
});


lazyRequireTask('html', './gulp/tasks/html', {
  src: paths.src.html,
  dest: paths.build.html
});


lazyRequireTask('libs', './gulp/tasks/libs', {
  dest: paths.build.js
});

lazyRequireTask('images', './gulp/tasks/images', {
  src: paths.src.images,
  dest: paths.build.images
});

lazyRequireTask('fonts', './gulp/tasks/fonts', {
  src: paths.src.fonts,
  dest: paths.build.fonts
});

gulp.task('build', gulp.series(
  'clean', gulp.parallel('js', 'libs', 'html', 'styles', 'images', 'fonts'))
);

gulp.task('watch', () => {
  $.livereload.listen();

  Object.keys(watchers).forEach(watcher => $.watch(
    watchers[watcher].path,
    gulp.series(watchers[watcher].task())
  ));
});

gulp.task('dev', gulp.series('build', 'watch'));



