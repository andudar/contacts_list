'use strict';

var
  express = require('express'),
  config = require('getconfig'),
  debug = require('debug')('server:server'),
  http = require('http'),
  favicon = require('serve-favicon'),
  cookieParser = require('cookie-parser'),
  log4js = require('log4js'),
  bodyParser = require('body-parser'),
  passport = require('passport'),

  log = require('./services/system/loggingService'),
  pathService = require('./services/pathService'),
  routeService = require('./services/routeService');



var app = express();

// view engine setup
app.set('views', pathService.templates());
app.set('view engine', 'hjs');

//distribute favicon
app.use(favicon(pathService.favIcon()));

//sets logger
//disable logging for testing cases
if(process.env.NODE_ENV !== 'test') {
    app.use(log4js.connectLogger(log4js.getLogger("http"), { level: 'auto' }));
}

log4js.configure(config.logging, {
  cwd: pathService.logDir()
});

app.use(bodyParser.json()); //read forms what were send by POST --> req.body
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());


// provide static files
app.use(express.static(pathService.pathFront()));


//require user model & passport
require('./db/models/user');
require('./libs/passport');


app.use(passport.initialize());

//routes
app.use('/', routeService.index);
app.use('/contacts', routeService.contacts);
app.use('/translate', routeService.translate);


//setting server
app.set('port', config.port);
const server = http.createServer(app);

server.listen(config.port, () => {
  log('app', 'info', `Server listening on ${config.port}`);
});


//error handler
const errInterceptor = require('./services/system/errorHandler');
app.use(errInterceptor);

module.exports = app;