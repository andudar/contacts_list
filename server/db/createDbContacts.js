"use strict";
//set path to config directory
process.env.NODE_CONFIG_DIR = '../config';

var
  mongoose = require('./../libs/mongoose'),
  async = require('async'),
  log = require('log4js').getLogger('db: creating users');

var
  people = require('./data/contacts.json');

async.series([
    open,
    dropCollection,
    requireModels,
    createUsers
  ],
  err => {
    if (err) {
      log.error(err);
    }
    close();
    process.exit(err ? 255 : 0);
  }
);

function open(cb) {
  mongoose.connection.on('open', cb);
}

function dropDatabase(cb) {
  var db = mongoose.connection.db;
  db.dropDatabase(cb);
}


function dropCollection(cb) {
  var model = require('./models/contact.js');
  model.remove(cb)
}

function requireModels(cb) {
  require('./models/contact.js');
  async.each(mongoose.models, (model, cb) => {
    model.ensureIndexes(cb);
  }, cb)
}

function createUsers(cb) {
  async.each(people,
    (person, cb) => {
      var currentUser = new mongoose.models.Contact(person);
      currentUser.save(cb)
    }, err => {
      if (err) {
        cb(err);
      } else {
        cb();
        log.info('created users successfully')
      }
    });
}

function close(cb) {
  mongoose.disconnect(cb);
  log.info('connection closed');
}

