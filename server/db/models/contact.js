'use strict';

const mongoose = require('./../../libs/mongoose');

var schema = mongoose.Schema({
  name: String,
  lastName: {
    type: String,
    unique: true,
    required: true
  },
  age: Number,
  address: {
    street: String,
    number: Number
  },
  position: String,
  extraInfo: {
    hobbies: Array,
    music: Array
  },
  updated_at: { type: Date, default: Date.now }
});


schema.methods.toJSON = function() {
  var obj = this.toObject();
  delete obj.__v;
  return obj
};

module.exports = mongoose.model('Contact', schema);