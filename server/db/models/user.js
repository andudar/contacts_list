'use strict';

var
  mongoose = require('./../../libs/mongoose'),
  crypto = require('crypto'),
  jwt = require('jsonwebtoken'),
  ValidationError = require('./../../error').ValidationError,
  config = require('config');


//at least one lowercase char, one uppercase char and one number
// length between 4 - 8
const regExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}/;

var schema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  hash: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  }
});

schema.methods.encryptPassword = function (password) {
  return crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha256').toString('hex');
};

//temporary disabled!
/*schema.virtual('password')
 .set(function (password) {
 this._plainPassword = password;
 this.salt = crypto.randomBytes(16).toString('hex');
 this.hash = this.encryptPassword(password);
 })
 .get(function () {
 return this._plainPassword;
 });*/

schema.methods.setPassword = function (password) {
  if(!regExp.test(password)) {
    throw new ValidationError(400, 'Your password is not valid');
  }

  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha256').toString('hex');
};

schema.methods.validPassword = function (password) {
  return this.encryptPassword(password) === this.hash;
};

schema.methods.toJSON = function () {
  var obj = this.toObject();
  delete obj.__v;
  delete obj.hash;
  delete obj.salt;
  return obj;
};

/*schema.statics.authorize = function(email, password) {
 var User = this;

 return User.findOne({email: email})
 .then(user => {
 if (!user) {
 return User.create({email: email, password: password})
 }
 if (user.checkPassword(password)) {
 return user;
 } else {
 throw new AuthError("Forbidden");
 }
 })
 };*/

schema.methods.generateJwt = function () {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
      _id: this._id,
      email: this.email,
      name: this.name,
      exp: parseInt(expiry.getTime() / 1000),
    },
    config.auth.secret);
};

module.exports = mongoose.model('User', schema);