/**
 * Created by andrii on 17.08.16.
 */
'use strict';

const
  util = require('util'),
  http = require('http');

//custom errors which inherit from error class

//http's response error
var _HttpError = function (status, message) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, _HttpError);

  this.status = status;
  this.message = message || http.STATUS_CODES[status] || "Error";
};

util.inherits(_HttpError, Error);
_HttpError.prototype.name = 'HttpError';


//user's authorization error
var _AuthError = function (status, message) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, _AuthError);

  this.status = status;
  this.message = message || http.STATUS_CODES[status] || "AuthError";
};

util.inherits(_AuthError, Error);
_AuthError.prototype.name = 'AuthError';


//server validation error
var _ValidationError = function (status, message) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, _ValidationError);

  this.status = status;
  this.message = message || http.STATUS_CODES[status] || "ValidationError";
};

util.inherits(_ValidationError, Error);
_ValidationError.prototype.name = 'ValidationError';


//database error
var _DatabaseError = function (status, message, err) {
  Error.apply(this, arguments);
  Error.captureStackTrace(this, _DatabaseError);

  let error = err || {};

  this.name = "DatabaseError";
  this.message = message;
  this.errors = error.errors;
  this.status = status;
};

util.inherits(_DatabaseError, Error);
_DatabaseError.prototype.name = 'DatabaseError';


module.exports = {
  HttpError: _HttpError,
  AuthError: _AuthError,
  DatabaseError: _DatabaseError,
  ValidationError: _ValidationError
};
