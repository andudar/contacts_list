'use strict';

const mongoose = require('mongoose');
const log = require('./../services/system/loggingService');
const config = require('getconfig');

// Use native Node promises
mongoose.Promise = global.Promise;

// connect to MongoDB
mongoose.connect(config.mongoose.uri)
  .then(() =>  {
      log('database', 'info', 'connection successful');
      log('database', 'info', `using db: ${config.mongoose.uri}`);
  })
  .catch(err => log('database', 'error', err));

mongoose.set('debug', (collectionName, method, query, doc) => {
    log('database', 'info', `${collectionName}, ${method}, ${JSON.stringify(query)}, ${JSON.stringify(doc)}`);
});

module.exports = mongoose;