/**
 * Created by andrii on 23.08.16.
 */
'use strict';

const
  passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  User = require('./../db/models/user'),

  strategySettings = {
    usernameField: 'email'
  };

passport.use(new LocalStrategy(strategySettings, (email, password, done) => {
  User.findOne({email: email}, (err, user) => {
    // if there are any errors, return the error
    if (err) {
      return done(err);
    }

    // Return if user not found in database
    if (!user) {
      return done(null, false, {
        message: 'User not found'
      });
    }

    if (!user.validPassword(password)) {
      return done(null, false, {
        message: 'Password is wrong'
      });
    }
    // If credentials are correct, return the user object
    return done(null, user);
  });

}));

// used to serialize the user for the session
passport.serializeUser(function (user, done) {
  done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});
