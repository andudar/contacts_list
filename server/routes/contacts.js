'use strict';
const
  router = require('express').Router(),
  Contact = require('./../db/models/contact.js'),
  DatabaseError = require('./../error').DatabaseError;


/* GET /contacts listing. */
router.get('/', (req, res, next) => {
  Contact.find((err, contacts) => {
    if (err) return next(err);
    res.json(contacts);
  });
});

/* GET /contacts listing with pagination */
router.get('/:itemsPerPage/:pageNumber', (req, res, next) => {
  var
    itemsPerPage = +req.params.itemsPerPage,
    pageNumber = req.params.pageNumber;

  //timeout for testing loading bar
  setTimeout(() => {
    Contact.find().count().then(len => {
      let length = len;

      Contact.find()
        .skip(itemsPerPage * (pageNumber - 1))
        .limit(itemsPerPage)
        .exec(function (err, contacts) {
          if (err) return next(err);
          res.json({
            data: contacts,
            length: length
          });
        });
    });

  }, 500); //TODO remove
});

/* POST /contacts */
router.post('/', (req, res, next) => {
  Contact.create(req.body, (err, newContact) => {
    if (err) {
      return next(new DatabaseError(404, "Can't save this contacts to database", err));
    }
    res.json('OK');
  });
});

/* GET /contacts/:id */
router.get('/:id', (req, res, next) => {
  Contact.findById(req.params.id, (err, contact) => {
    if (err) {
      return next(new DatabaseError(404, "Can't find this contact in a database", err));
    }
    res.json(contact);
  });
});

/* PUT /contacts/:id */
router.put('/:id', (req, res, next) => {
  //it needs this approach in some reason: {_id: req.params.id}
  Contact.findOneAndUpdate({_id: req.params.id}, req.body, err => {
    if (err) {
      return next(new DatabaseError(404, "Can't save this contact to a database"));
    }
    res.json('OK');
  });
});

/* DELETE /contacts/:id */
router.delete('/:id', (req, res, next) => {
  Contact.findByIdAndRemove({_id: req.params.id}, err => {
    if (err) {
      return next(new DatabaseError(404, "Can't delete this user"));
    }
    res.json('OK');
  });
});

module.exports = router;
