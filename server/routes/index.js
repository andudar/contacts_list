'use strict';

const
  express = require('express'),
  router = express.Router(),
  jwt = require('express-jwt'),
  profileService = require('./../services/profileService'),
  authenticationService = require('./../services/authenticationService'),
  config = require('getconfig');


const
  auth = jwt({
  secret: config.auth.secret,
  userProperty: 'payload'
});

// authentication routers
router.post('/register', authenticationService.register, authenticationService.sendEmailInvitation);
router.post('/login', authenticationService.login);

// secure route
router.get('/profile', auth, profileService.profileRead);


module.exports = router;
