'use strict';

const
  router = require('express').Router(),
  translate = require('google-translate-api');

/* POST /translate  */
router.post('/', (req, res, next) => {
  let
    phrase = req.query['phrase'] || '',
    lang = req.query['lang'] || 'en';

  translate(phrase, { to: lang }).then(data => {
    //send translation & translated language back
    res.json({
      translation: data.text,
      from: data.from.language.iso
    });
  }).catch(err => next(err));
});

module.exports = router;
