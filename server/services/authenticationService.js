/**
 * Created by andrii on 25.08.16.
 */
'use strict';

const
  passport = require('passport'),
  User = require('./../db/models/user.js'),
  error = require('./../error/index'),
  log = require('./system/loggingService'),
  sendEmail = require('./mailService').sendEmail,

  //custom errors
  AuthError = error.AuthError,
  DatabaseError = error.DatabaseError;

const register = (req, res, next) => {
  if (!req.body.name || !req.body.email || !req.body.password) {
    return next(new AuthError(400, "All fields required"));
  }

  let user = new User();

  user.name = req.body.name;
  user.email = req.body.email;

  user.setPassword(req.body.password);

  //if request is valid try to save user in database
  //if saving ended successfully send token back to the user
  user.save(function (err) {
    if (err) {
      return next(new DatabaseError(400, err.message.name, err));
    }
    var
      token;

    token = user.generateJwt();
    res.status(200);
    res.json({
      "token": token
    });
    next();
  });
};

const _sendEmailInvitation = (req, res, next) => {

  let
    receiverEmail = req.body.email,
    receiverName = req.body.name,

    options = {
      appName: 'contacts_list',
      mainPage: 'http://localhost:4000/#/home/invitation',
      replyEmail: 'decidoro@gmail.com',
      name: receiverName,
      title: 'Hi from the app!'
    };

  sendEmail(receiverEmail, 'invitation', options)
    .then(data => {
      log('email', 'info', `email was send to ${receiverEmail}`);
    })
    .catch(err => {
      log('email', 'error', `can't send email to ${receiverEmail}`);
      return next(new AuthError(500, `Can't send an email`));
    });

};

const login = (req, res, next) => {
  //server validation for not complete user object
  if (!req.body.email || !req.body.password) {
    return next(new AuthError(400, "All fields required"));
  }

  passport.authenticate('local', function (err, user, info) {
    var
      token;

    // if passport throws/catches an error
    if (err) {
      return next(new AuthError(404, 'Problem with authorization'));
    }

    // If a user has been found
    if (user) {
      token = user.generateJwt();
      res.status(200);
      res.json({
        "token": token
      });
    } else {
      // If a user has not been found
      return next(new DatabaseError(404, 'User not found or have a wrong credentials'));
    }
  })(req, res);

};

module.exports = {
  register: register,
  sendEmailInvitation: _sendEmailInvitation,
  login: login
};
