/**
 * Created by andrii on 12.09.16.
 */
"use strict";

const
  SENDGRID_API_KEY = require('./../constants/SENDGRID_API_KEY.json').api_key,
  sg = require('sendgrid')(SENDGRID_API_KEY),
  fs = require('fs'),
  config = require('getconfig'),
  Hogan = require('hogan.js'),
  pathService = require('./pathService'),
  SENDER = config.credentials.email;

const themes = {
  invitation: {
    path: 'invitation.hjs',
    subject: 'Invitation email',
  }
};

const _createTemplate = theme => {
  let
    template = fs.readFileSync(pathService.emailTemplates() + theme, 'utf-8');
  return Hogan.compile(template);
};

const _sendEmail = (to, type, options) => {
  //create request object for email
  let request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: {
      personalizations: [
        {
          to: [
            {
              email: to,
            },
          ],
          subject: themes[type].subject,
        },
      ],
      from: {
        email: SENDER,
      },
      content: [
        {
          type: 'text/html',
          value: _createTemplate(themes[type].path).render(options),
        },
      ],
    },
  });

  return sg.API(request);
};


module.exports = {
  sendEmail: _sendEmail
};
