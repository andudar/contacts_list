/**
 * Created by andrii on 14.08.15.
 */
'use strict';

let
  config = require('getconfig'),
  path = require('path');

let
  root = './',
  front = path.join(config.front),
  favicon = path.join(config.front,config.favicon),
  templates = path.join(root, config.templates.root),
  emailTemplates = path.join(root, config.templates.emails),
  logDir = path.join(root, config.logging.outputDir);

module.exports = {
  pathFront: () => front,
  favIcon: () => favicon,
  templates: () => templates,
  emailTemplates: () => emailTemplates,
  logDir: () => logDir
};
