/**
 * Created by andrii on 25.08.16.
 */
'use strict';
const
  User = require('./../db/models/user'),

  AuthError = require('./../error').AuthError,
  DatabaseError = require('./../error').DatabaseError;

module.exports.profileRead = function(req, res, next) {
  // if no user id exists in the jwt return an error
  if (!req.payload._id) {
    return next(new AuthError(401, 'UnauthorizedError: profile is private'));
  } else {
    // otherwise continue
    User
      .findById(req.payload._id)
      .exec(function(err, user) {
        if(err) {
          return next(new DatabaseError(404, "User not found"))
        }
        res.status(200).json(user);
      });
  }
};