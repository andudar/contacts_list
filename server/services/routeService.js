/**
 * Created by andrii on 14.08.15.
 */

'use strict';
const
  contacts = require('./../routes/contacts'),
  translate = require('./../routes/translate'),
  index = require('./../routes/index');

module.exports = {
  contacts: contacts,
  translate: translate,
  index: index
};
