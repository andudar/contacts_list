/**
 * Created by andrii on 06.09.16.
 */
'use strict';

const errorHandler = require('errorhandler');
const log = require('./loggingService');


const _sendError = (err, res) => {
  let
    errorStatus = err.status || err.code; //err.code in case of mongo errors

  res.status(errorStatus);
  res.json(err);
};

module.exports = (() => {

  return (err, req, res, next) => {

    //authentication & unauthorized errors
    if (err.name === 'UnauthorizedError' || err.name === 'AuthError') {
      log('auth', 'error', err);
      _sendError(err, res);
    }

    //database errors
    else if (err.name === 'CastError' || err.name === 'DatabaseError' || err.name === 'ValidationError') {
      log('database', 'error', err);
      _sendError(err, res);
    }

    //build-in errors handler
    else if (process.env.NODE_ENV === 'development') {
      errorHandler()(err, req, res, next);
    }
  }
})();