/**
 * Created by andrii on 12.09.16.
 */
'use strict';

const
  logger = require('log4js');

//custom logger disabled for test environment
const _log = (name, type, message) => {
  if (process.env.NODE_ENV !== 'test') {
    let opts = {
      name: name || 'logger',
      type: type || 'info',
      message: message || 'something happened'
    };

    let
      log = logger.getLogger(opts.name);

    log[opts.type](opts.message);
  }
};

module.exports = _log;
