/**
 * Created by andrii on 09.09.16.
 */
//process.env.NODE_ENV = 'test';
'use strict';

const
  mongoose = require("mongoose");

const Contact = require('../../../db/models/contact');

const
  chai = require('chai'),
  chaiHttp = require('chai-http'),
  server = require('../../../app'),
  should = chai.should();

chai.use(chaiHttp);

describe('Contact route', () => {
  beforeEach(done => { // empty the database
    Contact.remove({}, err => {
      done();
    });
  });

  /*
   * Test the GET contacts/ route
   */
  describe('GET contacts/', () => {
    it('it should GET all the contacts', done => {
      chai.request(server)
        .get('/contacts/6/1')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.length.should.be.eql(0);
          done();
        });
    });
  });


  /*
   * Test the POST contacts/ route
   */
  describe('POST contacts/', () => {
    it('it should POST a person to the contacts collection', done => {
      let person = {
        name: "Richard",
        lastName: "Williams",
        age: 25
      };

      chai.request(server)
        .post('/contacts')
        .send(person)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.equal('OK');
          done();
        });
    });

    it('it should not POST a person without lastName field to the contacts collection', done => {
      let person = {
        name: "James",
        age: 30
      };

      chai.request(server)
        .post('/contacts')
        .send(person)
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('lastName');
          res.body.errors.lastName.should.have.property('kind').eql('required');
          done();
        });
    });

  });


  /*
   * Test the GET contacts/:id route
   */
  describe('GET contacts/:id', () => {
    it('it should GET a person by the given id', done => {
      let person = new Contact({
        name: "Anthony",
        lastName: "Barbary",
        age: 40
      });

      person.save((err, person) => {
        chai.request(server)
          .get(`/contacts/${person.id}`)
          .send(person)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.should.have.property('lastName');
            res.body.should.have.property('age');
            res.body.should.have.property('_id').eql(person.id);
            done();
          });
      });

    });
  });


  /*
   * Test the PUT contacts/:id route
   */
  describe('PUT contacts/:id', () => {
    it('it should UPDATE a person with the given id', done => {
      let person = new Contact({
        name: "Anthony",
        lastName: "Barbary",
        age: 40
      });

      person.save((err, book) => {
        chai.request(server)
          .put('/contacts/' + person.id)
          .send({name: "Richard", age: 41})
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.equal('OK');
            done();
          });
      });
    });
  });


  /*
   * Test the /DELETE/:id route
   */
  describe('DELETE contacts/:id person', () => {
    it('it should DELETE a person with the given id', done => {
      let person = new Contact({
        name: "Anthony",
        lastName: "Barbary",
        age: 40
      });

      person.save((err, person) => {
        chai.request(server)
          .delete('/contacts/' + person.id)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.equal('OK');
            done();
          });
      });
    });
  });

});