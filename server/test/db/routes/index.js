/**
 * Created by andrii on 09.09.16.
 */
//process.env.NODE_ENV = 'test';
'use strict';

const
  mongoose = require("mongoose"),
  config = require('config');


const User = require('../../../db/models/user');


let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../../app');
let should = chai.should();

chai.use(chaiHttp);


describe('Auth route', () => {
  beforeEach(done => { // empty the database
    User.remove({}, err => {
      done();
    });
  });

  /*
   * Test the POST register/ route
   */

  describe('POST register/', () => {

    it('it should register a new user', done => {

      let user = {
        name: "Richard Branson",
        email: "abc@cba",
        password: "123aA"
      };

      chai.request(server)
        .post('/register')
        .send(user)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('token');
          done();
        });
    });

    it('it should not register a new user with an invalid password', done => {

      let user = {
        name: "Richard Branson",
        email: "abc@cba",
        password: "123" //invalid password
      };

      chai.request(server)
        .post('/register')
        .send(user)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.message.should.be.equal('Your password is not valid');
          done();
        });
    });

    it('it should not register a new user without an email', done => {

      let user = {
        name: "Richard Branson",
        email: "", // it's required field
        password: "123aA"
      };

      chai.request(server)
        .post('/register')
        .send(user)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.message.should.be.equal('All fields required');
          done();
        });
    });
  });


  /*
   * Test the POST login/ route
   */
  describe('POST login/', () => {
    it('it should login with a new user', done => {

      let
        user = new User();

      user.name = "Richard Branson";
      user.email = "abc@cba";
      user.setPassword("123aA");

      let credentials = {
        email: "abc@cba",
        password: "123aA"
      };


      user.save((err, user) => {
        chai.request(server)
          .post('/login')
          .send(credentials)
          .end((err, res) => {

            //check that password was set
            user.should.have.property('salt');
            user.should.have.property('hash');

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('token');
            done();
          });
      });


    });

    it('user should not login with a wrong credentials', done => {
      //create new user
      let
        user = new User();

      user.name = "Richard Branson";
      user.email = "abc@cba";
      user.setPassword("123aA");

      let credentials = {
        email: "abc@cba",
        password: "123" // wrong password
      };


      user.save((err, user) => {
        chai.request(server)
          .post('/login')
          .send(credentials)
          .end((err, res) => {
            res.should.have.status(404);
            res.body.should.be.a('object');

            //check error properties
            res.body.should.have.property('name');
            res.body.name.should.be.equal('DatabaseError');
            res.body.should.have.property('message');
            res.body.message.should.be.equal('User not found or have a wrong credentials');

            done();
          });
      });


    });
  });

});